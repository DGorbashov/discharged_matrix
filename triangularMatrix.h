#pragma once
#include <iostream>
#include <vector>
using namespace std;


class triangularMatrix {
private:
	int   Size;
	int** Matrix;
public:
	triangularMatrix(int n) {
		Size   = n;
		Matrix = new int*[n*n];

		for (int k = 0; k < Size; k++) {
			Matrix[k] = new int[Size - k];
			for (int j = 0; j <= Size - 1 - k; j++)
				Matrix[k][j] = rand() % 10;
		}
	}

	void Print() {
		for (int k = 0; k < Size; k++) {
			for (int j = 0; j < k; j++)
				cout << "\t";
			for (int j = 0; j <= Size - 1 - k; j++)
				cout << Matrix[k][j] << "\t";
			cout << endl;
		}
	}

	int* operator[](size_t k) {
		if (0 <= k && k < Size) {
			return Matrix[k];
		}
	}

	void MultVector(vector<int> B) {
		for (int i = 0; i < Size; i++) {
			for (int j = 0; j < Size; j++) Matrix[i][j] *= B[i];
		}
	}


};